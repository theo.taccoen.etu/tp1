const name = 'Regina';
const url = `images/${name.toLowerCase()}.jpg`;
console.log(url);

let html = '';



const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];

data.sort(function(a,b) {
    if (a.name < b.name) return -1;
    if (a.name > b.name) return 1;
    return 0;
});

//let newData = [...data];

const newData = data.filter(({name}) => name.split('i').length == 3);

html = newData.reduce( (acc, {name, image, price_large, price_small}) => {
    return acc +
    `<article class="pizzaThumbnail">
	<a href="${image}">
    <img src="${image}"/>
    <section>
        <h4>${name}</h4>
        <ul>
            <li>Prix petit format : ${price_small}€</li>
            <li>Prix grand format : ${price_large}€</li>
        </ul>
    </section>
	</a>
    </article>`;
    
}, '');
document.querySelector(".pageContent").innerHTML = html;