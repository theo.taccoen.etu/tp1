"use strict";

var name = 'Regina';
var url = "images/".concat(name.toLowerCase(), ".jpg");
console.log(url);
var html = '';
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
data.sort(function (a, b) {
  if (a.name < b.name) return -1;
  if (a.name > b.name) return 1;
  return 0;
});
var newData = [].concat(data);
newData = data.filter(function (_ref) {
  var name = _ref.name;
  return name.split('i').length == 3;
});
html = newData.reduce(function (acc, _ref2) {
  var name = _ref2.name,
      image = _ref2.image,
      price_large = _ref2.price_large,
      price_small = _ref2.price_small;
  return acc + "<article class=\"pizzaThumbnail\">\n\t<a href=\"".concat(image, "\">\n    <img src=\"").concat(image, "\"/>\n    <section>\n        <h4>").concat(name, "</h4>\n        <ul>\n            <li>Prix petit format : ").concat(price_small, "\u20AC</li>\n            <li>Prix grand format : ").concat(price_large, "\u20AC</li>\n        </ul>\n    </section>\n\t</a>\n    </article>");
}, '');
document.querySelector(".pageContent").innerHTML = html;
//# sourceMappingURL=main.js.map